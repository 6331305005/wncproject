var express = require('express');
var cors = require('cors');
var app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors({ origin: true }));
app.use(cors());

app.use('/',express.static('../dist'));

app.listen(process.env.PORT || 8244, function () {
    console.log('Node app is running on 8244');
});
