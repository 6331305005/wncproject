const express = require("express");
const router = express.Router();
const dbConn = require("../database");
const mysql = require("mysql");

// Get All
router.get("/getAll", (req, res) => {
  let sql = 'SELECT * FROM UNIT';
  let query = dbConn.query(sql, (err, results) => {
    if (err) throw err;
    res.send(JSON.stringify(results));
  });
});


module.exports = router;