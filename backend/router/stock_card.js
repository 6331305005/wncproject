const express = require("express");
const router = express.Router();
const dbConn = require("../database");
const mysql = require("mysql");

// Get All
router.get("/getAll", (req, res) => {
  let sql = 'SELECT *,FS.stock_name AS from_stock_name,TS.stock_name AS to_stock_name, DATEDIFF(CURDATE(),sc_exp) AS countdown FROM STOCK_CARD JOIN MEDICENT JOIN UNIT JOIN TYPE JOIN STOCK AS FS JOIN STOCK AS TS ON STOCK_CARD.med_id = MEDICENT.med_id AND STOCK_CARD.to_stock = TS.stock_id AND STOCK_CARD.from_stock = FS.stock_id AND MEDICENT.type_id = TYPE.type_id AND MEDICENT.unit_id = UNIT.unit_id ORDER BY STOCK_CARD.id';
  let query = dbConn.query(sql, (err, results) => {
    if (err) throw err;
    console.log(results);
    res.send(JSON.stringify(results));
  });
});

// Get EXP && MED_ID
router.get("/get/:med_id/:sc_exp", function (req, res) {
  let med_id = req.params.med_id;
  let sc_exp = req.params.sc_exp;
  dbConn.query('SELECT * FROM STOCK_CARD WHERE med_id =? AND sc_exp = ?', [med_id, sc_exp], function (error, results, fields) {
    try {
      return res.send({ error: false, data: results, message: 'Get TYPE by med_id ' + med_id });
    } catch (err) {
      console.log(err);
      return res.send({ error: true, data: results, message: err + "" });
    }
  });
});
router.get("/getSSStock/:med_id/:status/:to_stock", function (req, res) {
  let med_id = req.params.med_id;
  let status = req.params.status;
  let to_stock = req.params.to_stock;
  dbConn.query('SELECT *,FS.stock_name AS from_stock_name,TS.stock_name AS to_stock_name FROM STOCK_CARD JOIN MEDICENT JOIN UNIT JOIN TYPE JOIN STOCK AS FS JOIN STOCK AS TS ON STOCK_CARD.med_id = MEDICENT.med_id AND STOCK_CARD.to_stock = TS.stock_id AND STOCK_CARD.from_stock = FS.stock_id AND MEDICENT.type_id = TYPE.type_id AND MEDICENT.unit_id = UNIT.unit_id WHERE STOCK_CARD.med_id =? AND STOCK_CARD.status =? AND STOCK_CARD.to_stock = ?', [med_id, status, to_stock], function (error, results, fields) {
    try {
      return res.send({ error: false, data: results });
    } catch (err) {
      console.log(err);
      return res.send({ error: true, data: results, message: err + "" });
    }
  });
});

router.get("/getSStock/:status/:to_stock", function (req, res) {
  let status = req.params.status;
  let to_stock = req.params.to_stock;
  dbConn.query('SELECT *,FS.stock_name AS from_stock_name,TS.stock_name AS to_stock_name FROM STOCK_CARD JOIN MEDICENT JOIN UNIT JOIN TYPE JOIN STOCK AS FS JOIN STOCK AS TS ON STOCK_CARD.med_id = MEDICENT.med_id AND STOCK_CARD.to_stock = TS.stock_id AND STOCK_CARD.from_stock = FS.stock_id AND MEDICENT.type_id = TYPE.type_id AND MEDICENT.unit_id = UNIT.unit_id WHERE STOCK_CARD.status =? AND STOCK_CARD.to_stock = ?', [status, to_stock], function (error, results, fields) {
    try {
      return res.send({ error: false, data: results });
    } catch (err) {
      console.log(err);
      return res.send({ error: true, data: results, message: err + "" });
    }
  });
});

// Get EXP && MED_ID
router.get("/getStatus/:status", function (req, res) {
  let status = req.params.status;
  dbConn.query('SELECT *,FS.stock_name AS from_stock_name,TS.stock_name AS to_stock_name FROM STOCK_CARD JOIN MEDICENT JOIN UNIT JOIN TYPE JOIN STOCK AS FS JOIN STOCK AS TS ON STOCK_CARD.med_id = MEDICENT.med_id AND STOCK_CARD.to_stock = TS.stock_id AND STOCK_CARD.from_stock = FS.stock_id AND MEDICENT.type_id = TYPE.type_id AND MEDICENT.unit_id = UNIT.unit_id WHERE STOCK_CARD.status =?', [status], function (error, results, fields) {
    try {
      return res.send({ error: false, data: results, message: 'Get TYPE by status ' + status });
    } catch (err) {
      console.log(err);
      return res.send({ error: true, data: results, message: err + "" });
    }
  });
});

// Get ID
router.get('/getByID/:med_id', function (req, res) {
  let med_id = req.params.med_id;
  dbConn.query('SELECT *,FS.stock_name AS from_stock_name,TS.stock_name AS to_stock_name FROM STOCK_CARD JOIN MEDICENT JOIN UNIT JOIN TYPE JOIN STOCK AS FS JOIN STOCK AS TS ON STOCK_CARD.med_id = MEDICENT.med_id AND STOCK_CARD.to_stock = TS.stock_id AND STOCK_CARD.from_stock = FS.stock_id AND MEDICENT.type_id = TYPE.type_id AND MEDICENT.unit_id = UNIT.unit_id WHERE MEDICENT.med_id = ?', [med_id], function (error, results, fields) {
    try {
      return res.send({ error: false, data: results, message: 'By med_id' + med_id });
    } catch (err) {
      console.log(err);
      return res.send({ error: true, data: results, message: err + "" });
    }
  });
});
// Get Name
router.get('/getByName/:med_name', function (req, res) {
  let med_name = req.params.med_name;
  dbConn.query('SELECT *,FS.stock_name AS from_stock_name,TS.stock_name AS to_stock_name FROM STOCK_CARD JOIN MEDICENT JOIN UNIT JOIN TYPE JOIN STOCK AS FS JOIN STOCK AS TS ON STOCK_CARD.med_id = MEDICENT.med_id AND STOCK_CARD.to_stock = TS.stock_id AND STOCK_CARD.from_stock = FS.stock_id AND MEDICENT.type_id = TYPE.type_id AND MEDICENT.unit_id = UNIT.unit_id WHERE MEDICENT.med_name = ?', [med_name], function (error, results, fields) {
    try {
      return res.send({ error: false, data: results, message: 'By med_name' + med_name });
    } catch (err) {
      console.log(err);
      return res.send({ error: true, data: results, message: err + "" });
    }
  });
});

// POST
router.post('/add', function (req, res) {
  let STOCK_CARD = req.body;
  console.log(req.body);
  dbConn.query("INSERT INTO STOCK_CARD SET ? ", STOCK_CARD, function (error, results, fields) {
    try {
      console.log(STOCK_CARD);
      return res.send({ error: false, data: results, message: 'New STOCK_CARD successfully.' });
    } catch (err) {
      console.log(err);
      return res.send({ error: true, data: results, message: err + "" });
    }

  });
});



//Put -- Update
router.put('/update/:med_id/:sc_exp', function (req, res) {
  let med_id = req.params.med_id;
  let sc_exp = req.params.sc_exp;
  let STOCK_CARD = req.body;
  dbConn.query("UPDATE STOCK_CARD SET ? WHERE med_id=? AND sc_exp =?", [STOCK_CARD, med_id, sc_exp], function (error, results, fields) {
    try {
      console.log(STOCK_CARD);
      return res.send({ error: false, data: results, message: 'Update STOCK_CARD ' + med_id + sc_exp });
    } catch (err) {
      console.log(err);
      return res.send({ error: true, data: results, message: err + "" });
    }

  });
});

module.exports = router;