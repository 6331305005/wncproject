const router = require("express").Router();

const medicentRouter = require("./medicent");
const authRouter = require("./auth");
const typeRouter = require("./type");
const unitRouter = require("./unit");
const stockcardRouter = require("./stock_card");
const conditionRouter = require("./condi");
const stockRouter = require("./stock");


router.use("/medicent", medicentRouter);
router.use("/auth", authRouter);
router.use("/type", typeRouter);
router.use("/unit", unitRouter);
router.use("/stockcard", stockcardRouter);
router.use("/condi", conditionRouter);
router.use("/stock", stockRouter);



module.exports = router;