const express = require("express");
const router = express.Router();
const dbConn = require("../database");
const mysql = require("mysql");

// Get All
router.get("/getAll",(req,res)=>{
    let sql = 'SELECT * FROM PRODUCT JOIN MEDICENT JOIN COMPANY JOIN WAREHOUSE JOIN UNIT JOIN TYPE ON PRODUCT.med_id = MEDICENT.med_id AND PRODUCT.c_id = COMPANY.c_id AND PRODUCT.wh_id = WAREHOUSE.wh_id AND MEDICENT.type_id = TYPE.type_id AND MEDICENT.unit_id = UNIT.unit_id';
    let query = dbConn.query(sql, (err, results) => {
      if(err) throw err;
      res.send(JSON.stringify( results ));
  }); 
});

// Get All
router.get("/all",(req,res)=>{
    let sql = 'SELECT * FROM PRODUCT JOIN MEDICENT JOIN COMPANY JOIN WAREHOUSE ON PRODUCT.med_id = MEDICENT.med_id AND PRODUCT.c_id = COMPANY.c_id AND PRODUCT.wh_id = WAREHOUSE.wh_id';
    let query = dbConn.query(sql, (err, results) => {
      if(err) throw err;
      res.send(JSON.stringify( results ));
  }); 
});

module.exports = router;