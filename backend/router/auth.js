const express = require("express");
const router = express.Router();
const db = require("../database");
const mysql = require("mysql");

const session = require("express-session");
const MySQLStore = require("express-mysql-session")(session);

const options = {
  host: 'se.mfu.ac.th',
  user: 'wncmfu2022',
  password: 'wnc123456',
  database: 'wncmfu2022_db'
};

const sessionStore = new MySQLStore(options); // create a new session store

router.use(
  session({
    secret: "your-secret-key",
    store: sessionStore, // use the session store here
    resave: false,
    saveUninitialized: false,
  })
);

const cookieParser = require("cookie-parser");
router.use(cookieParser());

router.post("/login", (req, res) => {
  const { username, password } = req.body;
  // Check if the username and password are correct
  // Query the database to find the user
  const query = `SELECT * FROM USERS WHERE user_username = '${username}' AND user_password = '${password}'`;
  db.query(query, (err, results) => {
    if (err) {
      console.error(err);
      res.status(500).json({ message: "An error occurred" });
    } else if (results.length === 0) {
      res.status(401).json({ message: "Invalid username or password" });
    } else {
      // Set the user ID in the session
      req.session.userId = results[0].id;

      // Return the user's id and username
      const user = {
        id: results[0].id,
        username: results[0].username,
      };
      res.status(200).json({ message: "Login successful", user });
    }
  });
});

router.post('/add', function (req, res) {
  let { user, pass } = req.body;
  console.log(req.body);
  dbConn.query("SELECT * FROM USERS WHERE user_username = ? AND user_password = ? ", [user , pass], function (error, results, fields) {
      try {
          console.log(USERS);
          return res.send({ error: false, data: results, message: 'New USERS successfully.' });
      } catch (err) {
          console.log(err);
          return res.send({ error: true, data: results, message: err + "" });
      }

  });
});

router.post("/logout", (req, res) => {
  req.session.destroy((error) => {
    if (error) {
      res.status(500).json({ message: "Error logging out." });
    } else {
      res.json({});
    }
  });
});

module.exports = router;
