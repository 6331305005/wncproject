const express = require("express");
const router = express.Router();
const dbConn = require("../database");
const mysql = require("mysql");

// Get All
router.get("/getAll", (req, res) => {
  let sql = 'SELECT * FROM TYPE';
  let query = dbConn.query(sql, (err, results) => {
    if (err) throw err;
    res.send(JSON.stringify(results));
  });
});

router.get('/getOne/:type_id', function (req, res) {
  let type_id = req.params.type_id;
  dbConn.query('SELECT * FROM TYPE WHERE type_id = ? ', [type_id], function (error, results, fields) {
      try {
          return res.send({ error: false, data: results, message: 'Get TYPE by type_id ' + type_id });
      } catch (err) {
          console.log(err);
          return res.send({ error: true, data: results, message: err + "" });
      }
  });
});

module.exports = router;