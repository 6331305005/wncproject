const express = require("express");
const router = express.Router();
const dbConn = require("../database");
const mysql = require("mysql");

// Get All Medicent
router.get('/getAll', async (req, res) => {
    try {
        let sql = 'SELECT * FROM MEDICENT JOIN UNIT JOIN CONDI JOIN TYPE ON MEDICENT.type_id = TYPE.type_id AND MEDICENT.unit_id = UNIT.unit_id AND MEDICENT.con_id = CONDI.con_id';
        dbConn.query(sql, (err, results) => {
            if (err) {
                console.log(err);
                return res.status(400).send();
            }
            res.send(JSON.stringify(results));
        });
    } catch (err) {
        console.log(err);
        return res.status(500).send();
    }
});

// Get ID
router.get('/getByID/:med_id', function (req, res) {
    let med_id = req.params.med_id;
    dbConn.query('SELECT * FROM MEDICENT JOIN UNIT JOIN TYPE JOIN CONDI ON MEDICENT.type_id = TYPE.type_id AND MEDICENT.unit_id = UNIT.unit_id AND MEDICENT.con_id = CONDI.con_id WHERE MEDICENT.med_id = ?', [med_id], function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'By med_id' + med_id});
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});
// Get Name
router.get('/getByName/:med_name', function (req, res) {
    let med_name = req.params.med_name;
    dbConn.query('SELECT * FROM MEDICENT JOIN UNIT JOIN TYPE JOIN CONDI ON MEDICENT.type_id = TYPE.type_id AND MEDICENT.unit_id = UNIT.unit_id AND MEDICENT.con_id = CONDI.con_id WHERE MEDICENT.med_name = ?', [med_name], function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'By med_name' + med_name});
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});


// Get type
router.get('/getOne/:type_id', function (req, res) {
    let type_id = req.params.type_id;
    dbConn.query('SELECT * FROM MEDICENT JOIN UNIT JOIN TYPE JOIN CONDI ON MEDICENT.type_id = TYPE.type_id AND MEDICENT.unit_id = UNIT.unit_id AND MEDICENT.con_id = CONDI.con_id WHERE MEDICENT.type_id = ?', [type_id], function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'By Type' + type_id});
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});

// Post
router.post('/add', function (req, res) {
    let MEDICENT = req.body;
    console.log(req.body);
    dbConn.query("INSERT INTO MEDICENT SET ? ", MEDICENT, function (error, results, fields) {
        try {
            console.log(MEDICENT);
            return res.send({ error: false, data: results, message: 'New MEDICENT successfully.' });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }

    });
});

//Put -- Update
router.put('/update/:med_id', function(req, res) {
    let med_id = req.params.med_id;
    let MEDICENT = req.body;
    dbConn.query("UPDATE MEDICENT SET ? WHERE med_id=?", [MEDICENT, med_id], function(error, results, fields) {
        try {
            console.log(MEDICENT);
            return res.send({ error: false, data: results, message: 'Update MEDICENT ' + med_id });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }

    });
});

module.exports = router;


