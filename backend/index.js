var cors = require("cors");
const bodyParser = require("body-parser");
const express = require("express");
const session = require("express-session");
const app = express();

const router = require("./router");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors({ origin: true }));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(
  session({
    secret: "your-secret-key",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }, // Note: set this to 'true' if using HTTPS
  })
);

app.use("/", router);

app.get("/backend", async (req, res) => {
  try {
    res.send({ message: "Backend Started!" });
  } catch (err) {
    console.log(err);
  }
});

// app.listen(8888, () => {
//   console.log("Server listening on port 8888");
// });


app.listen(process.env.PORT || 8243, function () {
  console.log('Node app is running on 8243');
});