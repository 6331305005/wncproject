const mysql = require("mysql");

const dbConn = mysql.createConnection({
    host: 'se.mfu.ac.th',
    user: 'wncmfu2022',
    password: 'wnc123456',
    database: 'wncmfu2022_db'
});

dbConn.connect((err) => {
    if (err) throw err;
    console.log('Mysql Connected...');
});

module.exports = dbConn;
